"use strict";

const expect = require("chai").expect;
const { performance } = require("perf_hooks");


const testCase = async () => {
	const { sayiOku } = require("../"); // imports

	// RAKAM OKU
	let birakam = "123004.560060";
	sayiOku(birakam).catch(error => console.error(error));

	// PARA OKU
	let bipara = "1231,99";
	sayiOku(bipara, "dolar").catch(error => console.error(error));
};

const startTest = async () => {
	let t0 = performance.now(); // zaman ölçüme başla
	await testCase();
	let t1 = performance.now(); // zaman ölçümü bitir

	console.log(`İşlem Süresi: ${t1 - t0} milisaniye`);

	return (t0 - t1);

};

describe("Performans Testi", () => {
	it(`100ms nin altında tamamlamalı`, async () => {
		let result = await startTest();
		expect(result).to.be.below(100);
	});
});