# Changelog
[Paketin] değişim detayları aşağıda belirtilmiştir.

## [1.1.4] - 2018.11.19
#### Değişti
- npm package script "prepublish" > "prepare" olarak değiştirildi.

## [1.1.3] - 2018.11.19
#### Tamir Edildi
- Paraoku: 0.00 ise "sıfır lira" şeklinde okusun.

## [1.1.2] - 2018.11.19
#### Tamir Edildi
- Paraoku: virgülden sonra ,00 varsa "kuruş" yazmasın.

## [1.1.1] - 2018.11.13
#### Eklendi
- Parabirimi küçük harflerle de verilse kabul etsin. "try" >> "TRY"
- Yeni para birimi:
  - "AFN" > Afganistan Para Birimi
  - "AZN" > Azerbaycan Para Birimi
  - "ARS" > Arjantin Pesosu
  - "PKR" > Pakistan Rupisi
  - "SAR" > Suudi Arabistan Riyali
#### Değişti
- Cli de okuması için tek bir sayı verilirse array yerine string dönsün.

## [1.0.1] - 2018.11.12
#### Değişti
- Lisans ISC den MIT e dönüştürüldü.

## [1.0.0] - 2018.11.12
#### Değişti
- Paketin adı ~~rakam-oku~~ iken **sayi-oku** yapıldı. (NodeJS Türkiye'ye teşekkürler.)

## [0.0.1] - 2018.11.10
### Eklendi
- Çalışacak hale getirildi.
- Paket olarak yayına sunuldu.

## [Unreleased] - 2018.11.4
### Eklendi
- npm init.

[Paketin]: https://gitlab.com/ulvido/sayi-oku
[1.0.0]: https://gitlab.com/ulvido/sayi-oku/tree/228980d662232ad6a70d3ae36889c2218c0bd638
