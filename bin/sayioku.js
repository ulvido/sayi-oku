#!/usr/bin/env node
"use strict";

const { sayiOku } = require("..");

// ilk iki argüman path. siliyoruz.
const args = process.argv.slice(2);

// FONKSIYON TANIMLAMALARI
const getArg = (arg, liste) => {
	let argIndex = liste.indexOf(arg);
	return (argIndex === -1) ? null : liste[argIndex + 1];
};

const removeArgFromList = (arg, liste) => {
	let argIndex = liste.indexOf(arg);
	liste.splice(argIndex, 1); // delete arg    // --para
	liste.splice(argIndex, 1); // delete value // TRY
	return liste;
};

// MAIN FONKSIYON
const oku = async (args) => {
	try {
		if (getArg("--para", args)) { // para varsa para oku
			const paraBirimi = getArg("--para", args);
			let sadeceRakamlar = removeArgFromList("--para", args);
			let result = await sayiOku(sadeceRakamlar, paraBirimi);
			if (result.length === 1) { // tek bir sayı varsa array yerine string dönsün
				return result.toString();
			}
			return result;
		} else { // yoksa normal oku
			
			let result = await sayiOku(args);
			if (result.length === 1) { // tek bir sayı varsa array yerine string dönsün
				return result.toString();
			}
			return result;
		}
	} catch (error) {
		return "Hata! CLI okuma başarısız!";
	}
};

// RUN
oku(args).then(console.log).catch(console.error);