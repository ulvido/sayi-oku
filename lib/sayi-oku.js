"use strict";

/** 
 * Verilen rakamı yazıya dönüştürür.
 * Tamsayı ve ondalık okuyabilir.
 * 
 * FLOW: sayı > matrix yap > içlerini oku > matrixi birleştir.
 * 
 * İkinci argüman olarak küresel para birimi verilirse
 * sayiOku(sayı,parakodu) > para okur. ÖR: sayiOku("3","EUR") => "üç euro"
 * 
 * Number kabul etmez.
 * Çünkü Javascript'te bir number en fazla 15 basamak olabilir.
 * 
 * String formatında:
 * Tamsayı max 450 basamaklı
 * Ondalık max 450,450 basamaklı sayı verilebilir.
 * 
 * Stringlerden oluşan bir array verilirse 
 * input > ["string", "string", "string"]
 * returns > ["okunmuşString", "okunmuşString", "okunmuşString"]
 */

class Sayi {

	constructor(sayi, para) {
		this.sayi = sayi; 					// String: tamsayı veya ondalık > , veya .
		this.para = para; 		// parakodu: ÖR: "TRY"
	}

	async sayiOku(num = this.sayi, para = this.para) { // interface

		let dataType = await this.checkDataType(num, para);

		switch (dataType) {
			case "NUMBER": {
				let result = "Hata! Number vermeyiniz.";
				return result;
			}
			case "PARAKODUYOK": {
				let result = "Hata! Parakodu geçersiz.";
				return result;
			}
			case "0": {
				let result = para ? `sıfır ${Sayi.paraBirimleri()[para]["birim"]}` : "sıfır";
				return result;
			}
			case "ARRAY": {
				let result = await this.arrayOku(num);
				return result;
			}
			case "TAMSAYI": {
				let result = para ? this.tamsayiParaOku(num, para) : this.tamsayiOku(num);
				return result;
			}
			case "ONDALIK": {
				let result = para ? this.ondalikParaOku(num, para) : this.ondalikSayiOku(num);
				return result;
			}
			case "BOŞ": {
				let error = new Error(`Hata! Boş string.`);
				return error.message;
			}
			case null: {
				let error = new Error(`Hata! Geçersiz data.`);
				return error.message;
			}
			default: {
				break;
			}
		}
	}

	async checkDataType(sayi, para) {
		try {
			let isvalid = await this.isValid(sayi);
			let istamsayi = await this.isTamsayi(sayi);
			let ispara = await this.isPara(para);

			if (typeof sayi === "number") {    // type Number ise reddet
				return "NUMBER";
			} else if (!ispara) {  // sıfır ise
				return "PARAKODUYOK";
			} else if (this.sayi === "0") {  // sıfır ise
				return "0";
			} else if (Array.isArray(sayi)) {  // array ise
				return "ARRAY";
			} else if (isvalid && istamsayi) {  // tamsayi ise
				return "TAMSAYI";
			} else if (isvalid && !istamsayi) { // ondalık ise
				return "ONDALIK";
			} else if (this.sayi === "") { // boş ise
				return "BOŞ";
			} else {
				return null;
			}
		} catch (error) {
			return error;
		}
	}

	async isValid(num) { // max 450 (nokta veya virgül) max 450 sayı
		try {
			let regexSema = /^(\d{1,450})?(\.|,)?\d{1,450}$/g;
			let result = await this.isRegexMatch(num, regexSema);
			return result;
		} catch (error) {
			return error;
		}
	}

	async isTamsayi(num) { // max 450, sadece sayı
		try {
			let regexSema = /^\d{1,450}$/g;
			let result = await this.isRegexMatch(num, regexSema);
			return result;
		} catch (error) {
			return error;
		}
	}

	async isPara(parakodu) { // para listede var mı
		try {
			if (parakodu === undefined || Sayi.paraBirimleri()[parakodu]) {
				return true;
			} else {
				return false;
			}
		} catch (error) {
			return error;
		}
	}

	async isRegexMatch(str, regexSema) {
		try {
			if (str.match(regexSema)) {
				return true;
			} else {
				return false;
			}
		}
		catch (error) {
			return error;
		}
	}

	// TAMSAYI
	async tamsayiOku(num) {
		try {
			const rakamToMatrix = await this.buildMatrixFromRakam(num);
			const okuMatrix = await this.okuRakamMatrix(rakamToMatrix);
			const matrixToString = await this.matrixToString(okuMatrix);
			const fixBirBin = await this.fixBirBin(matrixToString);
			let result = fixBirBin;
			return result;
		} catch (error) {
			return error;
		}
	}

	async tamsayiParaOku(num, para) {
		try {
			let okunmusPara = await this.tamsayiOku(num);
			let birimVarmi = okunmusPara ? Sayi.paraBirimleri()[para]["birim"] : "";
			let result = `${okunmusPara} ${birimVarmi}`.trim();
			return result;
		} catch (error) {
			return error;
		}
	}

	// ONDALIK
	async ondalikSayiOku(num) {
		try {
			let { slice1, slice2, noktalamaIsareti } = await this.noktalamaIsaretindenAyir(num);

			let okunus1 = this.tamsayiOku(slice1);
			let noktalamaOkunus = this.okunusText("noktalama", noktalamaIsareti);
			let okunus2 = this.sifirlarlaOku(slice2);

			let promises = await Promise.all([okunus1, noktalamaOkunus, okunus2]);
			let result = `${promises[0]} ${promises[1]} ${promises[2]}`;
			return result;
		} catch (error) {
			return error;
		}
	}

	async ondalikParaOku(num, para) {
		try {
			let { slice1, slice2 } = await this.noktalamaIsaretindenAyir(num);

			let okunus1 = await this.tamsayiOku(slice1) === "" ? "sıfır" : this.tamsayiOku(slice1);
			let paraBirimLira = Sayi.paraBirimleri()[para]["birim"];
			let okunus2 = await this.kurusOku(slice2);
			let paraBirimKurus = okunus2 === "" ? "" : Sayi.paraBirimleri()[para]["altbirim"];


			let promises = await Promise.all([okunus1, paraBirimLira, okunus2, paraBirimKurus]);
			let result = `${promises[0]} ${promises[1]} ${promises[2]} ${promises[3]}`.trim();
			return result;
		} catch (error) {
			return error;
		}
	}

	// HELPER FUNCTIONS
	async noktalamaIsaretindenAyir(num) {
		let isNokta = await this.isRegexMatch(num, /[.]/g);
		let noktalamaIsareti = isNokta ? "." : ",";

		let noktalamaIndex = num.indexOf(noktalamaIsareti);

		let slice1 = num.slice(0, noktalamaIndex);    // virgülden önce
		let slice2 = num.slice(noktalamaIndex + 1);   // virgülden sonra
		return { slice1, slice2, noktalamaIsareti };
	}

	async sifirlarlaOku(num) { // virgülden sonrasını oku > sayı için
		try {
			let rakamKismi = num.replace(/0+$/, ""); // 0012345000 => 0012345
			let ilkSifirlar = num.replace(/[1-9]\d*/, ""); // 0012345000 => 00

			let okunus = await this.tamsayiOku(rakamKismi);
			const result = `${"sıfır ".repeat(ilkSifirlar.length)}${okunus}`;
			return result;
		} catch (error) {
			return error;
		}
	}

	async kurusOku(num) { // virgülden sonrasını oku > para için
		try {
			let rakamKismi = num.replace(/0+$/, ""); // 0012345000 => 0012345

			if (rakamKismi.length > 2) {
				return new Error(`Hata! Virgülden sonra max 2 hane olabilir.`);
			}

			let rakamKurus = rakamKismi.padEnd(2, "0");
			let result = await this.tamsayiOku(rakamKurus);
			return result;
		} catch (error) {
			return error;
		}
	}

	async arrayOku(array) {
		try {
			let tmp = [];
			for (let i = 0; i < array.length; i++) {
				let okunus = await this.sayiOku(array[i]);
				tmp.push(okunus);
			}
			let result = tmp;
			return result;
		} catch (error) {
			return error;
		}
	}

	// BUILDER FUNCTIONS
	async buildMatrixFromRakam(sayi) {
		Array.matrixFromRakam = (num) => {
			// ÖRNEK : 
			// 
			// input: 
			// "1745163524"
			//
			// output:
			//  [ [ '0', '0', '1',"3" ],     // milyar > baş taraf sıfırla dolmuş.
			//   [ '7', '4', '5', "2" ],     // milyon
			//   [ '1', '6', '3', "1" ],     // bin
			//   [ '5', '2', '4', "0" ] ]    // yüzler onlar birler sonek
			//
			// sayı uzunluğunu 3 ün katları yapmak için baş tarafı 0 la doldur.
			let rakamLength = num.length;
			let sifirAdedi = (rakamLength % 3) === 0 ? 0 : 3 - (rakamLength % 3);
			const yenirakam = `${"0".repeat(sifirAdedi)}${num}`;
			const rakamToListe = Array.from(yenirakam);
			const reverseListe = rakamToListe.reverse();
			const basamakAdedi = yenirakam.length / 3; // binler, milyonlar,..
			let tmp = [];
			for (let i = 0; i < basamakAdedi; i++) {
				let binlikBolum = [];
				for (let j = 0; j < 3; j++) {
					binlikBolum[j] = reverseListe.pop(); // <-- 1745163524 <--
				}
				// sonek için listeye bir rakam daha ekliyoruz. milyon, milyar vs.
				if (binlikBolum.join("") === "000") {
					binlikBolum.push("0"); // binlik komple sıfırsa sonek olmasın
				} else {
					binlikBolum.push((basamakAdedi - (i + 1)).toString());
				}
				tmp[i] = binlikBolum;
			}
			return tmp;
		};

		try {
			let result = Array.matrixFromRakam(sayi);
			return result;
		} catch (error) {
			return error;
		}
	}

	async okuRakamMatrix(matrix) {
		let okuMatrix = async (matrix) => {
			// ÖRNEK:
			// 
			// input;		
			// 	[ [ '0', '0', '1' ],    // milyar
			//   [ '7', '4', '5' ],     // milyon
			//   [ '1', '6', '3' ],     // bin
			//   [ '5', '2', '4' ] ]    // yüzler onlar birler
			// 
			// output;
			// [ [ '', '', 'bir', 'milyar' ],
			// [ 'yediyüz', 'kırk', 'beş', 'milyon' ],
			// [ 'yüz', 'altmış', 'üç', 'bin' ],
			// [ 'beşyüz', 'yirmi', 'dört', '' ] ]
			//
			let tmp = [];
			for (let i = 0; i < matrix.length; i++) {
				let yuzler = this.okunusText("yuzler", matrix[i][0]);
				let onlar = this.okunusText("onlar", matrix[i][1]);
				let birler = this.okunusText("birler", matrix[i][2]);
				let sonek = this.okunusText("sonek", matrix[i][3]);

				tmp[i] = await Promise.all([yuzler, onlar, birler, sonek]);
			}
			return tmp;
		};

		try {
			let result = okuMatrix(matrix);
			return result;
		} catch (error) {
			return error;
		}
	}

	async matrixToString(matrix) {
		// ÖRNEK:
		// 		
		// input;
		// [ [ '', '', 'bir', 'milyar' ],
		// [ 'yediyüz', 'kırk', 'beş', 'milyon' ],
		// [ 'yüz', 'altmış', 'üç', 'bin' ],
		// [ 'beşyüz', 'yirmi', 'dört', '' ] ]
		// 
		// output:
		// bir milyar yediyüzkırkbeş milyon yüzaltmışüç bin beşyüzyirmidört
		// 
		try {
			let basamaklaRakamiAyir = matrix
				.map(data => [[...data.slice(0, 3)].join(""), data[3]].join(" ").trim());
			let result = basamaklaRakamiAyir.join(" ").trim();
			return result;
		}
		catch (error) {
			return error;
		}
	}

	// FIXES
	async fixBirBin(str) { // istisna fix. Türkçe'de bir bin diye okumuyoruz.
		try {
			if (str.slice(0, 7) === "bir bin") {
				let result = str.slice(4);
				return result;
			} else if (str.match(/milyon bir bin/g)) {
				let result = str.replace("milyon bir bin", "milyon bin");
				return result;
			} else {
				return str;
			}
		}
		catch (error) {
			return error;
		}
	}

	// CONSTANTS
	async okunusText(tipi, rakam) {
		const okunusListesi = {
			"noktalama": {
				".": "nokta",
				",": "virgül",
			},
			"birler": {
				"0": "",
				"1": "bir",
				"2": "iki",
				"3": "üç",
				"4": "dört",
				"5": "beş",
				"6": "altı",
				"7": "yedi",
				"8": "sekiz",
				"9": "dokuz"
			},
			"onlar": {
				"0": "",
				"1": "on",
				"2": "yirmi",
				"3": "otuz",
				"4": "kırk",
				"5": "elli",
				"6": "altmış",
				"7": "yetmiş",
				"8": "seksen",
				"9": "doksan"
			},
			"yuzler": {
				"0": "",
				"1": "yüz",
				"2": "ikiyüz",
				"3": "üçyüz",
				"4": "dörtyüz",
				"5": "beşyüz",
				"6": "altıyüz",
				"7": "yediyüz",
				"8": "sekizyüz",
				"9": "dokuzyüz"
			},
			"sonek": {
				"0": "",
				"1": "bin",
				"2": "milyon",
				"3": "milyar",
				"4": "trilyon",
				"5": "katrilyon",
				"6": "kentilyon",
				"7": "seksilyon",
				"8": "septilyon",
				"9": "oktilyon",
				"10": "nonilyon",
				"11": "desilyon",
				"12": "andesilyon",
				"13": "dodesilyon",
				"14": "tredesilyon",
				"15": "kattordesilyon",
				"16": "kendesilyon",
				"17": "seksdesilyon",
				"18": "septendesilyon",
				"19": "oktodesilyon",
				"20": "novemdesilyon",
				"21": "vicintilyon",
				"22": "anvicintilyon",
				"23": "dovicintilyon",
				"24": "trevicintilyon",
				"25": "kattorvicintilyon",
				"26": "kenvicintilyon",
				"27": "seksvicintilyon",
				"28": "septenvicintilyon",
				"29": "oktovicintilyon",
				"30": "novemvicintilyon",
				"31": "tricintilyon",
				"32": "antricintilyon",
				"33": "dotricintilyon",
				"34": "tretricintilyon",
				"35": "kattortricintilyon",
				"36": "kentricintilyon",
				"37": "sekstricintilyon",
				"38": "septentricintilyon",
				"39": "oktotricintilyon",
				"40": "novemtricintilyon",
				"41": "katracintilyon",
				"42": "ankatracintilyon",
				"43": "dokatracintilyon",
				"44": "trekatracintilyon",
				"45": "kattorkatracintilyon",
				"46": "kenkatracintilyon",
				"47": "sekskatracintilyon",
				"48": "septenkatracintilyon",
				"49": "oktokatracintilyon",
				"50": "novemkatracintilyon",
				"51": "kenkacintilyon",
				"52": "ankenkacintilyon",
				"53": "dokenkacintilyon",
				"54": "trekenkacintilyon",
				"55": "kattorkenkacintilyon",
				"56": "kenkenkacintilyon",
				"57": "sekskenkacintilyon",
				"58": "septenkenkacintilyon",
				"59": "oktokenkacintilyon",
				"60": "novemkenkacintilyon",
				"61": "seksacintilyon",
				"62": "anseksacintilyon",
				"63": "doseksacintilyon",
				"64": "treseksacintilyon",
				"65": "kattorseksacintilyon",
				"66": "kenseksacintilyon",
				"67": "sekssexacintilyon",
				"68": "septenseksacintilyon",
				"69": "oktoseksacintilyon",
				"70": "novemseksacintilyon",
				"71": "septuacintilyon",
				"72": "anseptuacintilyon",
				"73": "doseptuacintilyon",
				"74": "treseptuacintilyon",
				"75": "kattorseptuacintilyon",
				"76": "kenseptuacintilyon",
				"77": "seksseptuacintilyon",
				"78": "septenseptuacintilyon",
				"79": "oktoseptuacintilyon",
				"80": "novemseptuacintilyon",
				"81": "oktocintilyon",
				"82": "anoktocintilyon",
				"83": "dooktocintilyon",
				"84": "treoktocintilyon",
				"85": "kattoroktocintilyon",
				"86": "kenoktocintilyon",
				"87": "seksoktocintilyon",
				"88": "septenoktocintilyon",
				"89": "oktooktocintilyon",
				"90": "novemoktocintilyon",
				"91": "nonacintilyon",
				"92": "annonacintilyon",
				"93": "dononacintilyon",
				"94": "trenonacintilyon",
				"95": "kattornonacintilyon",
				"96": "kennonacintilyon",
				"97": "seksnonacintilyon",
				"98": "septennonacintilyon",
				"99": "oktononacintilyon",
				"100": "novemnonacintilyon",
				"101": "sentilyon",
				"102": "senuntilyon",
				"103": "sendotilyon",
				"104": "sentretilyon",
				"105": "senkattortilyon",
				"106": "senkentilyon",
				"107": "sensekstilyon",
				"108": "senseptentilyon",
				"109": "senoktotilyon",
				"110": "sennovemtilyon",
				"111": "sendesilyon",
				"112": "senandesilyon",
				"113": "sendodesilyon",
				"114": "sentredesilyon",
				"115": "senkattordesilyon",
				"116": "senkendesilyon",
				"117": "senseksdesilyon",
				"118": "senseptendesilyon",
				"119": "senoktodesilyon",
				"120": "sennovemdesilyon",
				"121": "senvicintilyon",
				"122": "senunvicintilyon",
				"123": "sendovicintilyon",
				"124": "sentrevicintilyon",
				"125": "senkattorvicintilyon",
				"126": "senkenvicintilyon",
				"127": "senseksvicintilyon",
				"128": "senseptenvicintilyon",
				"129": "senoktovicintilyon",
				"130": "sennovemvicintilyon",
				"131": "sentricintilyon",
				"132": "senuntricintilyon",
				"133": "sendotricintilyon",
				"134": "sentretricintilyon",
				"135": "senkattortricintilyon",
				"136": "senkentricintilyon",
				"137": "sensekstricintilyon",
				"138": "senseptentricintilyon",
				"139": "senoktotricintilyon",
				"140": "sennovemtricintilyon",
				"141": "senkatracintilyon",
				"142": "senunkatracintilyon",
				"143": "sendokatracintilyon",
				"144": "sentrekatracintilyon",
				"145": "senkattorkatracintilyon",
				"146": "senkenkatracintilyon",
				"147": "sensekskatracintilyon",
				"148": "senseptenkatracintilyon",
				"149": "senoktokatracintilyon"
			}
		};

		try {
			let result = okunusListesi[tipi][rakam];
			return result;
		}
		catch (error) {
			return error;
		}
	}

	static paraBirimleri() {
		let paralar = {
			"TRY": {
				"birim": "lira",
				"altbirim": "kuruş",
				"isim": "Türk Lirası",
				"sembol": "₺"
			},
			"EUR": {
				"birim": "euro",
				"altbirim": "sent",
				"isim": "Avrupa Para Birimi",
				"sembol": "€"
			},
			"USD": {
				"birim": "dolar",
				"altbirim": "sent",
				"isim": "Amerikan Doları",
				"sembol": "$"
			},
			"GBP": {
				"birim": "sterlin",
				"altbirim": "peni",
				"isim": "Büyük Britanya Poundu",
				"sembol": "£"
			},
			"JPY": {
				"birim": "yen",
				"altbirim": "sen",
				"isim": "Japon Yeni",
				"sembol": "¥"
			},
			"CHF": {
				"birim": "frank",
				"altbirim": "santim",
				"isim": "İsviçre Frankı",
				"sembol": "SFr."
			},
			"ROU": {
				"birim": "ruble",
				"altbirim": "kopek",
				"isim": "Rus Rublesi",
				"sembol": "₽"
			},
			"AFN": {
				"birim": "afgani",
				"altbirim": "pul",
				"isim": "Afganistan Para Birimi",
				"sembol": "؋"
			},
			"AZN": {
				"birim": "manat",
				"altbirim": "kepik",
				"isim": "Azerbaycan Para Birimi",
				"sembol": "₼"
			},
			"ARS": {
				"birim": "peso",
				"altbirim": "sentavo",
				"isim": "Arjantin Pesosu",
				"sembol": "$"
			},
			"PKR": {
				"birim": "rupi",
				"altbirim": "paisa",
				"isim": "Pakistan Rupisi",
				"sembol": "Rs"
			},
			"SAR": {
				"birim": "riyal",
				"altbirim": "halala",
				"isim": "Suudi Arabistan Riyali",
				"sembol": "ر.س"
			},
		};
		return paralar;
	}
}

const sayiOku = async (sayi, para) => { // creator function
	let paraUpper = para ? para.toUpperCase() : para;
	let yeniSayi = new Sayi(sayi, paraUpper);
	const result = yeniSayi.sayiOku();
	return result;
};

module.exports = {
	sayiOkuInterface: sayiOku,
	paraBirimleriInterface: Sayi.paraBirimleri()
};