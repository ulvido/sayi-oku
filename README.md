# sayi-oku
Bu [paket] verilen sayıyı yazıya çevirir.

Sıfır dependency içerir.

## Kurulum
### Local
```
$ npm i -P sayi-oku
```
### Global
```
$ npm i -g sayi-oku
```

## Kullanım
async - await temelinde inşa edilmiştir.

Verilen sayıyı yazıya dönüştürür.
- Tamsayı ve ondalık okuyabilir. sayiOku("3.5") => "üç nokta beş"
  
İkinci argüman olarak küresel para birimi verilirse para okur.
- sayiOku( sayi, parakodu ) => para okur. ÖR: sayiOku( "3", "EUR" ) => "üç euro"
 
###### Geçerli para kodları:
- "TRY" > Türk Lirası
- "EUR" > Avrupa Birliği Parası
- "USD" > Amerikan Doları
- "GBP" > Büyük Britanya Poundu
- "JPY" > Japon Yeni
- "CHR" > İsviçre Frankı
- "ROU" > Rus Rublesi
- "AFN" > Afganistan Para Birimi
- "AZN" > Azerbaycan Para Birimi
- "ARS" > Arjantin Pesosu
- "PKR" > Pakistan Rupisi
- "SAR" > Suudi Arabistan Riyali

Number kabul etmez.
- Çünkü Javascript'te bir number en fazla 15 basamak olabilir.
 
String formatında:
- Tamsayı max 450 basamaklı
- Ondalık max 450,450 basamaklı sayı verilebilir.
 
Stringlerden oluşan bir array verilirse:
- input > ["string", "string", "string"]
- returns > ["okunmuşString", "okunmuşString", "okunmuşString"]
 

```javascript
const { sayiOku } = require("sayi-oku");

// SAYI OKU

// temel kullanım
sayiOku("3425").then(console.log).catch(console.error);
//
// "üç bin dörtyüzyirmibeş"



// tamsayı ve ondalık okuyabilir. virgül veya nokta destekler.
sayiOku("34,25").then(console.log).catch(console.error);
//
// "otuzdört virgül yirmibeş"



// PARA OKU
/**
 * tamsayı ve ondalık para okumayı destekler. 
 * virgül veya nokta kullanılabilir.
 * okunacak sayının yanında para kodu verilmelidir.
 * YENİ: parabirimini büyük veya küçük vermeniz sorun olmaz. "try" = "TRY"
 */
sayiOku("34,25", "EUR").then(console.log).catch(console.error);
//
// "otuzdört euro yirmibeş sent"



// ARRAY - tamsayı oku
let liste = ["3", "5", "8", "10"];
sayiOku(liste).then(console.log).catch(console.error);
//
// ["üç", "beş", "sekiz", "on"]



// Array - para oku
let listePara = (["3.1", "5,01", "8.80", "01.010"]);
sayiOku(listePara, "TRY").then(console.log).catch(console.error);
//
// ["üç lira on kuruş", "beş lira bir kuruş", "sekiz lira seksen kuruş", "bir lira bir kuruş"]
```


## CLI Kullanımı

### LOCAL kurulu ise:
###### Sayıoku
```
$ ./node_modules/.bin/sayioku 32 54,01 10.8954
```
###### Paraoku --para TRY,EUR,USD,GBP,JPY,CHR,ROU
```
$ ./node_modules/.bin/sayioku 10.25 55,99 --para EUR 898 41
```
### GLOBAL kurulu ise:
###### Sayıoku
```
$ sayioku 32 54,01 10.8954
```
###### Paraoku --para TRY,EUR,USD,GBP,JPY,CHR,ROU
```
$ sayioku 10.25 55,99 898 41 --para JPY
```


## Eslint

#### Lint hatalarını göster:
```
$ ./node_modules/.bin/eslint .
```

#### Belirli bir dosyadaki lint hatalarını göster:
```
$ ./node_modules/.bin/eslint ./index.js
```

#### Lint hatalarını tamir et:
```
$ ./node_modules/.bin/eslint . --fix
```

## Test
```
$ npm test
```

## Kendime Notlar
kaynak: [w3schools](https://www.w3schools.com/js/js_numbers.asp)

- Matematiksel bir işleme sokmadan önce HERZAMAN number olduğundan emin ol.

#### Integers (numbers without a period or exponent notation) are accurate up to 15 digits.
```javascript
var x = 999999999999999;   // x -> 999999999999999 (15 hane doğru)
var y = 9999999999999999;  // y -> 10000000000000000 (16 hanede şaşırıyor)
```

#### ONDALIKLAR ile işlem YAP-MA! hatalı gösterir. tamsayı yapıp sonra bölersen bu hata olmaz.
```javascript
var x = 0.2 + 0.1;         // x -> 0.30000000000000004
// ÇÖZÜM: önce tamsayı yap. işlemini yap sonra böl.
var x = (0.2 * 10 + 0.1 * 10) / 10;       // x -> 0.3
```

#### STRINGLER ile işlem YAP-MA!. Toplamda bi sonuç çıkar ama seni yanıltır.
```javascript
var x = "10";
var y = "20";
var z = x + y;    // z -> 1020 (string)
```

#### YA DA:
```javascript
var x = 10;    // bu number olsa bile hatalı gelir.
var y = "20";
var z = x + y;    // z -> 1020 (string)
```

#### ÇOK ÖNEMLİ: javascriptte işlem soldan sağa doğru yapılır.
```javascript
var x = 10;
var y = 20;
var z = "Sonuç: " + x + y;  // z -> "Sonuç: 1020"
// burada soldaki stringin üstüne x'i ekler. sonra bu yeni oluşan stringe y'yi ekler
```

#### ÇOK ÖNEMLİ-reloaded: javascriptte işlem soldan sağa doğru yapılır.
```javascript
var x = 10;
var y = 20;
var z = "30";
var result = x + y + z; // z -> "3030" (string)
// burada sayıya sayı ekler 30 yapar. ama sonraki 30 string olduğundan string toplaması yapar.
```

#### ÇARPMA, BÖLME, ÇIKARMA stringlerde çalışır. toplama ÇALIŞMAZ.
- "10" * "10" = 100  ✔
- "10" / "10" = 1    ✔
- "10" - "10" = 0    ✔
- "10" + "10" = 1010 ✖

- typeof NaN;     // returns "number" (bu da çok sakat)
- e harfi içeren sayı olabiliyor ona dikkat. // ÖR: 123e5


[paket]: https://gitlab.com/ulvido/sayi-oku