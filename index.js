"use strict";

const {
	sayiOkuInterface,
	paraBirimleriInterface } = require("./lib/sayi-oku");

module.exports = {
	sayiOku: sayiOkuInterface,
	paraBirimleri: paraBirimleriInterface
};